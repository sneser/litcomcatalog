﻿using System;
using Ninject;

namespace KatalogLitcom.Model
{
    public partial class SQLRepository : IRepository
    {
        [Inject]
        public kataloLitcomDataContext Db { get; set; }

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
    }
}
