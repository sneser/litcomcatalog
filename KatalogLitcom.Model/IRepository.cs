﻿using System.Collections.Generic;
using System.Linq;


namespace KatalogLitcom.Model
{
    public interface IRepository
    {
        #region Справочники

        #region CategoryForScladIzd 

        IQueryable<categoryForSkladIzd> categoryForSkladIzds { get; }

        bool createNewCategory(categoryForSkladIzd instanse);

        bool updateCategory(categoryForSkladIzd instanse);

        bool deleteCategory(int categID);

        categoryForSkladIzd getCategoryByID(int CategID);

        IList<categoryForSkladIzd> getCategoryByParentID(int parendID);

        #endregion

        #region dirIzd

        IQueryable<dirIzd> dirIzds { get; }

        bool createNewIzd(dirIzd instanse);

        bool updateIzd(dirIzd instance);

        bool deleteIzd(int izdID);

        IList<dirIzd> getIzdByCategory(int categID);

        dirIzd getIzdByID(int izdID);

        IList<dirIzd> getIzdByIndex(string index);

        #endregion

        #region dirStatusPartIzd

        IQueryable<dirStatusPartIsd> dirStatusPartIzds { get; }

        bool createDirStatusPartIzd(dirStatusPartIsd instance);

        bool updateDirStatusPartIzd(dirStatusPartIsd instance);

        bool deleteDirStatusPartIzd(int comutID);

        dirStatusPartIsd getDirStatusByID(int statID);

        #endregion

        #region dirSeries

        IQueryable<dirSeries> dirSeriess { get; }

        bool createDirSeries(dirSeries instance);

        bool updateDirSeries(dirSeries instance);

        bool deleteDirSeries(int recNomID);

        dirSeries getSeriesByID(int recNomID);

        #endregion

        #region dirOrg

        IQueryable<dirOrg> dirOrgs { get; }

        bool createDirOrg(dirOrg instance);

        bool updateDirOrg(dirOrg instance);

        bool deleteDirOrg(int recNomID);

        dirOrg getDirOrgByID(int orgID);

        IList<dirOrg> getDirOrgByRegion(int regionID);

        #endregion

        #region dirRegion

        IQueryable<dirRegions> dirRegionss { get; }

        bool createDirRegion(dirRegions instance);

        bool updateDirRegion(dirRegions instance);

        bool deleteDirRegon(int recNomID);

        dirRegions getDirRegionByID(int orgID);

        dirRegions getDirRegionOrg(int orgID);

        #endregion

        #region dirDiscountCustomer

        IQueryable<dirDiscountCustomer> dirDiscountCustomers { get; }

        bool createDirDiscountCustomer(dirDiscountCustomer instance);

        bool updateDirDiscountCustomer(dirDiscountCustomer instance);

        bool deleteDirDiscountCustomer(int recNomID);

        dirDiscountCustomer getdirDiscountCustomerByID(int discountID);

        IList<dirDiscountCustomer> getDiscountByName(string nameDisc);

        #endregion

        #region dirRLK

        IQueryable<dirRLK> dirRLKs { get; }

        bool createDirRLK(dirRLK instance);

        bool updateDirRLK(dirRLK instance);

        bool deleteDirRLK(int recNomID);

        dirRLK getDirRLKByID(int rlkID);

        #endregion

        #region regDataPriseIzd

        IQueryable<regDataPriseIzd> regDataPriseIzds { get; }

        #endregion 

        #endregion

        #region таблици Mappings

        #region comutStatusIzd

        IQueryable<comutStatusIzd> comutStatusIzds { get; }

        bool createNewComutStatusIzd(comutStatusIzd instanse);

        bool updateComutStatusIzd(comutStatusIzd instanse);

        bool deleteComutStatusIzd(int mappingID);

        comutStatusIzd getComutStatusIzdByID(int mappingID);

        IList<comutStatusIzd> getStatusByIzd(int izdID);

        #endregion

        #region comutRLKAndIzd

        IQueryable<comutRlkAndIzd> comutRlkAndIzds { get; }

        bool createNewComutRlkAndIzd(comutRlkAndIzd instanse);

        bool updateComutRlkAndIzd(comutRlkAndIzd instanse);

        bool deleteComutRlkAndIzd(int mappingID);

        comutRlkAndIzd getСomutRlkAndIzdByID(int mappingID);

        #endregion

        #region ComutIzdAndSeries

        IQueryable<ComutIzdAndSeries> ComutIzdAndSeriess { get; }

        bool createNewComutIzdAndSeries(ComutIzdAndSeries instanse);

        bool updateComutIzdAndSeries(ComutIzdAndSeries instanse);

        bool deleteComutIzdAndSeries(int mappingID);

        ComutIzdAndSeries getComutIzdAndSeriesByID(int mappingID);

        #endregion

        #region comutCustomerDiscountCategIzd

        IQueryable<comutCustomerDiscountCategizd> comutCustomerDiscountCategizds { get; }

        bool createNewcomutCustomerDiscountCategizd(comutCustomerDiscountCategizd instanse);

        bool updatecomutCustomerDiscountCategizd(comutCustomerDiscountCategizd instanse);

        bool deletecomutCustomerDiscountCategizd(int mappingID);

        comutCustomerDiscountCategizd getComutCustomerDiscountCategizdByID(int mappingID);

        #endregion

        #endregion

        #region Таблици детализации

        #region GaleryForIzd

        IQueryable<GaleryForIzd> GaleryForIzds { get; }

        bool createNewGaleryForIzd(GaleryForIzd instanse);

        bool updateGaleryForIzd(GaleryForIzd instanse);

        bool deleteGaleryForIzd(int mappingID);

        GaleryForIzd getGaleryForIzdByID(int mappingID);

        IList<GaleryForIzd> getGaleryForIzdByIzd(int izdID);

        #endregion

        #region DetailIzd

        IQueryable<DetailIzd> DetailIzds { get; }

        bool createNewDetailIzd(DetailIzd instanse);

        bool updateDetailIzd(DetailIzd instanse);

        bool deleteDetailIzd(int mappingID);

        DetailIzd getDetailIzdByID(int mappingID);

        IList<DetailIzd> getDetailIzdByIzd(int izdID);

        #endregion

        #endregion
    }
}
