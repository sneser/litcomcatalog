﻿using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

namespace KatalogLitcom.Model
{
    public partial class SQLRepository
    {
        public IQueryable<dirIzd> dirIzds
        {
            get
            {
                return Db.dirIzd;
            }
        }

        public bool createNewIzd(dirIzd instanse)
        {
            if(instanse.id == 0)
            {
                Db.dirIzd.InsertOnSubmit(instanse);
                try
                {
                    Db.dirIzd.Context.SubmitChanges();
                }
                catch(ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirIzd.Context.SubmitChanges();
                        }catch(ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }                    
                }
                return true;
            }
            return false;
        }

        public bool updateIzd(dirIzd instance)
        {
            dirIzd cache = Db.dirIzd.FirstOrDefault(p => p.id == instance.id);
            
            if(cache != null)
            {
                cache.idCateg = instance.idCateg;
                cache.myIndex = instance.myIndex;
                cache.nameIzd = instance.nameIzd;
                cache.sizeIzd = instance.sizeIzd;
                cache.imgFile = instance.imgFile;
                cache.pathToInstalInstructions = instance.pathToInstalInstructions;
                cache.visibleIzd = instance.visibleIzd;
                try
                {
                    Db.dirIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirIzd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool deleteIzd(int izdID)
        {
            dirIzd instance = Db.dirIzd.FirstOrDefault(p => p.id == izdID);
            if(instance != null)
            {
                Db.dirIzd.DeleteOnSubmit(instance);
                try
                {
                    Db.dirIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirIzd.Context.SubmitChanges();
                        }
                        catch(ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public IList<dirIzd> getIzdByCategory(int categID)
        {
            return Db.dirIzd.Where(p => p.idCateg == categID).ToList();
        }

        public dirIzd getIzdByID(int izdID)
        {
            return Db.dirIzd.FirstOrDefault(p => p.id == izdID);
        }

        public IList<dirIzd> getIzdByIndex(string index)
        {
            return Db.dirIzd.Where(p => p.myIndex.Contains(index)).ToList();
        }
    }
}
