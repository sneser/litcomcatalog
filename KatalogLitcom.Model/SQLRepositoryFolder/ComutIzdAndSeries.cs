﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogLitcom.Model
{
    public partial class SQLRepository
    {
        public IQueryable<ComutIzdAndSeries> ComutIzdAndSeriess
        {
            get
            {
                return Db.ComutIzdAndSeries;
            }
        }

        public bool createNewComutIzdAndSeries(ComutIzdAndSeries instanse)
        {
            if(instanse.comutID == 0)
            {
                Db.ComutIzdAndSeries.InsertOnSubmit(instanse);
                try
                {
                    Db.ComutIzdAndSeries.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.ComutIzdAndSeries.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool updateComutIzdAndSeries(ComutIzdAndSeries instanse)
        {
            ComutIzdAndSeries cache = Db.ComutIzdAndSeries.FirstOrDefault(p => p.comutID == instanse.comutID);
            if(cache != null)
            {
                cache.dirIzd = instanse.dirIzd;
                cache.dirSeries = instanse.dirSeries;
                try
                {
                    Db.ComutIzdAndSeries.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.ComutIzdAndSeries.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool deleteComutIzdAndSeries(int mappingID)
        {
            ComutIzdAndSeries instansce = Db.ComutIzdAndSeries.FirstOrDefault(p => p.comutID == mappingID);
            if(instansce != null)
            {
                Db.ComutIzdAndSeries.DeleteOnSubmit(instansce);
                try
                {
                    Db.ComutIzdAndSeries.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.ComutIzdAndSeries.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public ComutIzdAndSeries getComutIzdAndSeriesByID(int mappingID)
        {
            return Db.ComutIzdAndSeries.FirstOrDefault(p => p.comutID == mappingID);
        }        
    }
}
