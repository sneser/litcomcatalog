﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogLitcom.Model
{
    public partial class SQLRepository
    {
        public IQueryable<GaleryForIzd> GaleryForIzds
        {
            get
            {
                return Db.GaleryForIzd;
            }
        }

        public bool createNewGaleryForIzd(GaleryForIzd instanse)
        {            
            if(instanse.galeryID == 0)
            {
                Db.GaleryForIzd.InsertOnSubmit(instanse);
                try
                {
                    Db.GaleryForIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.GaleryForIzd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return true;
        }

        public bool updateGaleryForIzd(GaleryForIzd instanse)
        {
            GaleryForIzd cache = Db.GaleryForIzd.FirstOrDefault(p => p.galeryID == instanse.galeryID);

            if(cache != null)
            {
                cache.dirIzd = instanse.dirIzd;
                cache.pathToImgGalery = instanse.pathToImgGalery;
                try
                {
                    Db.GaleryForIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.GaleryForIzd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool deleteGaleryForIzd(int mappingID)
        {
            GaleryForIzd instance = Db.GaleryForIzd.FirstOrDefault(p => p.galeryID == mappingID);

            if(instance != null)
            {
                Db.GaleryForIzd.DeleteOnSubmit(instance);
                try
                {
                    Db.GaleryForIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.GaleryForIzd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public GaleryForIzd getGaleryForIzdByID(int mappingID)
        {
            return Db.GaleryForIzd.FirstOrDefault(p => p.galeryID == mappingID);
        }

        public IList<GaleryForIzd> getGaleryForIzdByIzd(int izdID)
        {
            return Db.GaleryForIzd.Where(p => p.izdID == izdID).ToList();
        }
    }
}
