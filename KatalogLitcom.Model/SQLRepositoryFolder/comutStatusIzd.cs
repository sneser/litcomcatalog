﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogLitcom.Model
{
    public partial class SQLRepository  
    {
        public IQueryable<comutStatusIzd> comutStatusIzds
        {
            get
            {
                return Db.comutStatusIzd;
            }
        }

        public bool createNewComutStatusIzd(comutStatusIzd instanse)
        {
            if(instanse.id == 0)
            {
                Db.comutStatusIzd.InsertOnSubmit(instanse);
                try
                {
                    Db.comutStatusIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.comutStatusIzd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool updateComutStatusIzd(comutStatusIzd instanse)
        {
            comutStatusIzd cache = Db.comutStatusIzd.FirstOrDefault(p => p.id == instanse.id);
            if(cache != null)
            {
                cache.dirIzd = instanse.dirIzd;
                cache.dirStatusPartIsd = instanse.dirStatusPartIsd;
                cache.isHasDiscount = instanse.isHasDiscount;
                try
                {
                    Db.comutStatusIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.comutStatusIzd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool deleteComutStatusIzd(int mappingID)
        {
            comutStatusIzd instanse = Db.comutStatusIzd.FirstOrDefault(p => p.id == mappingID);
            if (instanse != null)
            {
                Db.comutStatusIzd.DeleteOnSubmit(instanse);
                try
                {
                    Db.comutStatusIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.comutStatusIzd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public comutStatusIzd getComutStatusIzdByID(int mappingID)
        {
            return Db.comutStatusIzd.FirstOrDefault(p => p.id == mappingID);
        }

        public IList<comutStatusIzd> getStatusByIzd(int izdID)
        {
            return Db.comutStatusIzd.Where(p => p.id_Izd == izdID).ToList();
        }
    }
}
