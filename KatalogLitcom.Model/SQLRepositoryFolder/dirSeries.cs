﻿using System.Data.Linq;
using System.Linq;

namespace KatalogLitcom.Model
{
    public partial class SQLRepository
    {
        public IQueryable<dirSeries> dirSeriess
        {
            get
            {
                return Db.dirSeries;
            }
        }

        public bool createDirSeries(dirSeries instance)
        {
            if(instance.seriesID == 0)
            {
                Db.dirSeries.InsertOnSubmit(instance);
                try
                {
                    Db.dirSeries.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirSeries.Context.SubmitChanges();
                        }
                        catch(ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool updateDirSeries(dirSeries instance)
        {
            dirSeries cache = Db.dirSeries.FirstOrDefault(p => p.seriesID == instance.seriesID);

            if(cache != null)
            {
                cache.seriesName = instance.seriesName;
                try
                {
                    Db.dirSeries.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirSeries.Context.SubmitChanges();
                        }
                        catch(ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool deleteDirSeries(int recNomID)
        {
            dirSeries instance = Db.dirSeries.FirstOrDefault(p => p.seriesID == recNomID);

            if(instance != null)
            {
                Db.dirSeries.DeleteOnSubmit(instance);
                try
                {
                    Db.dirSeries.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirSeries.Context.SubmitChanges();
                        }
                        catch(ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public dirSeries getSeriesByID(int recNomID)
        {
            return Db.dirSeries.FirstOrDefault(p => p.seriesID == recNomID);
        }
    }
}
