﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogLitcom.Model
{
    public partial class SQLRepository
    {
        public IQueryable<comutRlkAndIzd> comutRlkAndIzds
        {
            get
            {
                return Db.comutRlkAndIzd;
            }
        }

        public bool createNewComutRlkAndIzd(comutRlkAndIzd instanse)
        {
            if(instanse.id == 0)
            {
                Db.comutRlkAndIzd.InsertOnSubmit(instanse);
                try
                {
                    Db.comutRlkAndIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.comutRlkAndIzd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool updateComutRlkAndIzd(comutRlkAndIzd instanse)
        {
            comutRlkAndIzd cache = Db.comutRlkAndIzd.FirstOrDefault(p => p.id == instanse.id);
            if(cache != null)
            {
                cache.comutStatusIzd = instanse.comutStatusIzd;
                cache.dirRLK = instanse.dirRLK;
                try
                {
                    Db.comutRlkAndIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.comutRlkAndIzd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool deleteComutRlkAndIzd(int mappingID)
        {
            comutRlkAndIzd instance = Db.comutRlkAndIzd.FirstOrDefault(p => p.id == mappingID);
            if(instance != null)
            {
                Db.comutRlkAndIzd.DeleteOnSubmit(instance);
                try
                {
                    Db.comutRlkAndIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.comutRlkAndIzd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public comutRlkAndIzd getСomutRlkAndIzdByID(int mappingID)
        {
            return Db.comutRlkAndIzd.FirstOrDefault(p => p.id == mappingID);
        }
    }
}
