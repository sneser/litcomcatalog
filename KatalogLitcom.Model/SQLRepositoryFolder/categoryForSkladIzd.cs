﻿using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

namespace KatalogLitcom.Model
{
    public partial class SQLRepository
    {
        public IQueryable<categoryForSkladIzd> categoryForSkladIzds
        {
            get
            {
                return Db.categoryForSkladIzd;
            }
        }

        public bool createNewCategory(categoryForSkladIzd instanse)
        {
            if(instanse.id == 0)
            {
                Db.categoryForSkladIzd.InsertOnSubmit(instanse);
                try
                {
                    Db.categoryForSkladIzd.Context.SubmitChanges();
                }
                catch(ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.categoryForSkladIzd.Context.SubmitChanges();
                        }
                        catch(ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool updateCategory(categoryForSkladIzd instanse)
        {
            categoryForSkladIzd cache = Db.categoryForSkladIzd.FirstOrDefault(p => p.id == instanse.id);
            if(cache != null)
            {
                cache.categoryName = instanse.categoryName;
                cache.parentID = instanse.parentID;
                try
                {
                    Db.categoryForSkladIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.categoryForSkladIzd.Context.SubmitChanges();
                        }
                        catch(ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool deleteCategory(int categID)
        {
            categoryForSkladIzd instanse = Db.categoryForSkladIzd.FirstOrDefault(p => p.id == categID);
            if(instanse != null)
            {
                Db.categoryForSkladIzd.DeleteOnSubmit(instanse);
                try
                {
                    Db.categoryForSkladIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.categoryForSkladIzd.Context.SubmitChanges();
                        }
                        catch(ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }                
                return true;
            }
            return false;
        }

        public categoryForSkladIzd getCategoryByID(int CategID)
        {
            return Db.categoryForSkladIzd.FirstOrDefault(p => p.id == CategID);
        }

        public IList<categoryForSkladIzd> getCategoryByParentID(int parendID)
        {
            return Db.categoryForSkladIzd.Where(p => p.parentID == parendID).ToList();
        }
    }
}
