﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogLitcom.Model
{
    public class returnedData
    {
        public int comutID { get; set; }
        public string NameIzd { get; set; }
        public string MyIndex { get; set; }
        public string SizeIzd { get; set; }
        public string StatusIzd { get; set; }
        public double PriseIzd { get; set; }
    }

    public partial class SQLRepository 
    {
        public IQueryable<regDataPriseIzd> regDataPriseIzds
        {
            get
            {
                return Db.regDataPriseIzd;
            }
        }

        public IList<regDataPriseIzd> getRegDataPrise()
        {
            var tmpSQlQuery = Db.regDataPriseIzd.Select(p => p).Join(Db.comutStatusIzd.Select(c => c), 
                                                                a => a.idComutStatusIzd, 
                                                                p => p.id, 
                                                                (a, p) => new {
                                                                    p.id,
                                                                    p.dirIzd.nameIzd,
                                                                    p.dirIzd.myIndex,
                                                                    p.dirIzd.sizeIzd,
                                                                    a.comutStatusIzd.dirStatusPartIsd.statuscomplete,
                                                                    a.priseIzd,
                                                                    a.dateRec}).Distinct().Max( k => k.dateRec);
            IList<regDataPriseIzd> tmpResult = new List<regDataPriseIzd>();
           /* foreach(var item in tmpSQlQuery)
            {
                tmpResult.Add(new regDataPriseIzd()
                {

                });
            }*/
            return tmpResult;
        }
    }
}
