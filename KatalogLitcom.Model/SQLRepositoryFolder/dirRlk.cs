﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogLitcom.Model
{ 
    public partial class SQLRepository
    {
        public IQueryable<dirRLK> dirRLKs
        {
            get
            {
                return Db.dirRLK;
            }
        }

        public bool createDirRLK(dirRLK instance)
        {
            if(instance.id != 0)
            {
                Db.dirRLK.InsertOnSubmit(instance);
                try
                {
                    Db.dirRLK.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirRLK.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool updateDirRLK(dirRLK instance)
        {
            dirRLK cache = Db.dirRLK.FirstOrDefault(p => p.id == instance.id);
            if(cache != null)
            {
                cache.namenPic = instance.namenPic;
                cache.RLK = instance.RLK;
                try
                {
                    Db.dirRLK.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirRLK.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool deleteDirRLK(int recNomID)
        {
            dirRLK instance = Db.dirRLK.FirstOrDefault(p => p.id == recNomID);
            if(instance != null)
            {
                Db.dirRLK.DeleteOnSubmit(instance);
                try
                {
                    Db.dirRLK.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirRLK.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public dirRLK getDirRLKByID(int rlkID)
        {
            return Db.dirRLK.FirstOrDefault(p => p.id == rlkID);
        }
    }
}
