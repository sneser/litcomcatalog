﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogLitcom.Model
{
    public partial class SQLRepository
    {
        public IQueryable<dirOrg> dirOrgs
        {
            get
            {
                return Db.dirOrg;
            }
        }

        public bool createDirOrg(dirOrg instance)
        {
            if(instance.id == 0)
            {
                Db.dirOrg.InsertOnSubmit(instance);
                try
                {
                    Db.dirOrg.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirOrg.Context.SubmitChanges();
                        }
                        catch(ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool updateDirOrg(dirOrg instance)
        {
            dirOrg cache = Db.dirOrg.FirstOrDefault(p => p.id == instance.id);
            if(cache != null)
            {
                cache.dirRegions = instance.dirRegions;
                cache.nameOrg = instance.nameOrg;
                cache.orgPresentative = instance.orgPresentative;
                cache.orgСity = instance.orgСity;
                cache.numTypeCalc = instance.numTypeCalc;
                cache.idCity = instance.idCity;
                cache.posTopManager = instance.posTopManager;
                cache.nameTopManager = instance.nameTopManager;
                cache.posTopManager = instance.posTopManager;
                try
                {
                    Db.dirOrg.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirOrg.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;                        
                    }
                }
                return true;
            }
            return false;
        }

        public bool deleteDirOrg(int recNomID)
        {
            dirOrg instance = Db.dirOrg.FirstOrDefault(p => p.id == recNomID);
            if(instance != null)
            {
                Db.dirOrg.DeleteOnSubmit(instance);
                try
                {
                    Db.dirOrg.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirOrg.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public dirOrg getDirOrgByID(int orgID)
        {
            return Db.dirOrg.FirstOrDefault(p => p.id == orgID);
        }

        public IList<dirOrg> getDirOrgByRegion(int regionID)
        {
            return Db.dirOrg.Where(p => p.idRegion == regionID).ToList();
        }
    }
}
