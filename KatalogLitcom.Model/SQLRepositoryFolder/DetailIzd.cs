﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogLitcom.Model
{
    public partial class SQLRepository
    {
        public IQueryable<DetailIzd> DetailIzds
        {
            get
            {
                return Db.DetailIzd;
            }
        }

        public bool createNewDetailIzd(DetailIzd instanse)
        {
            if(instanse.detailID == 0)
            {
                Db.DetailIzd.InsertOnSubmit(instanse);
                try
                {
                    Db.DetailIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.DetailIzd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool updateDetailIzd(DetailIzd instanse)
        {
            DetailIzd cache = Db.DetailIzd.FirstOrDefault(p => p.detailID == instanse.detailID);
            if(cache != null)
            {
                cache.dirIzd = instanse.dirIzd;
                cache.descriptionBig = instanse.descriptionBig;
                cache.descriptionShort = instanse.descriptionShort;
                try
                {
                    Db.DetailIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.DetailIzd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool deleteDetailIzd(int mappingID)
        {
            DetailIzd instance = Db.DetailIzd.FirstOrDefault(p => p.detailID == mappingID);
            if(instance != null)
            {
                Db.DetailIzd.DeleteOnSubmit(instance);
                try
                {
                    Db.DetailIzd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.DetailIzd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public DetailIzd getDetailIzdByID(int mappingID)
        {
            return Db.DetailIzd.FirstOrDefault(p => p.detailID == mappingID);
        }

        public IList<DetailIzd> getDetailIzdByIzd(int izdID)
        {
            return Db.DetailIzd.Where(p => p.izdID == izdID).ToList();
        }
    }
}
