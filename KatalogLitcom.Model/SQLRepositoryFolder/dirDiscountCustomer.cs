﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogLitcom.Model
{
    public partial class SQLRepository
    {
        public IQueryable<dirDiscountCustomer> dirDiscountCustomers
        {
            get
            {
                return Db.dirDiscountCustomer;
            }
        }

        public bool createDirDiscountCustomer(dirDiscountCustomer instance)
        {
            if(instance.discountID == 0)
            {
                Db.dirDiscountCustomer.InsertOnSubmit(instance);
                try
                {
                    Db.dirDiscountCustomer.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirDiscountCustomer.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool updateDirDiscountCustomer(dirDiscountCustomer instance)
        {
            dirDiscountCustomer cache = Db.dirDiscountCustomer.FirstOrDefault(p => p.discountID == instance.discountID);
            if(cache != null)
            {
                cache.discountNom = instance.discountNom;
                cache.discontName = instance.discontName;
                try
                {
                    Db.dirDiscountCustomer.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirDiscountCustomer.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool deleteDirDiscountCustomer(int recNomID)
        {
            dirDiscountCustomer instance = Db.dirDiscountCustomer.FirstOrDefault(p => p.discountID == recNomID);
            if(instance != null)
            {
                Db.dirDiscountCustomer.DeleteOnSubmit(instance);
                try
                {
                    Db.dirDiscountCustomer.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirDiscountCustomer.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public dirDiscountCustomer getdirDiscountCustomerByID(int discountID)
        {
            return Db.dirDiscountCustomer.FirstOrDefault(p => p.discountID == discountID);
        }

        public IList<dirDiscountCustomer> getDiscountByName(string nameDisc)
        {
            return Db.dirDiscountCustomer.Where(p => p.discontName.Contains("%" + nameDisc + "%")).ToList();
        }
    }
}
