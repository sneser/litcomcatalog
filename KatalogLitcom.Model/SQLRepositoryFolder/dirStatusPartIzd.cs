﻿using System.Data.Linq;
using System.Linq;

namespace KatalogLitcom.Model
{
    public partial class SQLRepository
    {
        public IQueryable<dirStatusPartIsd> dirStatusPartIzds
        {
            get
            {
                return Db.dirStatusPartIsd;
            }
        }

        public bool createDirStatusPartIzd(dirStatusPartIsd instance)
        {
            if(instance.id == 0)
            {
                Db.dirStatusPartIsd.InsertOnSubmit(instance);
                try
                {
                    Db.dirStatusPartIsd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirStatusPartIsd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool updateDirStatusPartIzd(dirStatusPartIsd instance)
        {
            dirStatusPartIsd cache = Db.dirStatusPartIsd.SingleOrDefault(p => p.id == instance.id);
            if(cache != null)
            {
                cache.statuscomplete = instance.statuscomplete;
                try
                {
                    Db.dirStatusPartIsd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirStatusPartIsd.Context.SubmitChanges();
                        }catch(ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool deleteDirStatusPartIzd(int comutID)
        {
            dirStatusPartIsd instance = Db.dirStatusPartIsd.FirstOrDefault(p => p.id == comutID);
            if(instance != null)
            {
                Db.dirStatusPartIsd.DeleteOnSubmit(instance);
                try
                {
                    Db.dirStatusPartIsd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirStatusPartIsd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public dirStatusPartIsd getDirStatusByID(int statID)
        {
            return Db.dirStatusPartIsd.FirstOrDefault(p => p.id == statID);
        }
    }
}
