﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogLitcom.Model
{
    public partial class SQLRepository
    {
        public IQueryable<dirRegions> dirRegionss
        {
            get
            {
                return Db.dirRegions;
            }
        }

        public bool createDirRegion(dirRegions instance)
        {
            if(instance.id == 0)
            {
                Db.dirRegions.InsertOnSubmit(instance);
                try
                {
                    Db.dirRegions.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirRegions.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }                    
                }
                return true;
            }
            return false;
        }

        public bool updateDirRegion(dirRegions instance)
        {
            dirRegions cache = Db.dirRegions.FirstOrDefault(p => p.id == instance.id);
            if(cache != null)
            {
                cache.nameRegion = instance.nameRegion;
                try
                {
                    Db.dirRegions.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirRegions.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool deleteDirRegon(int recNomID)
        {
            dirRegions instance = Db.dirRegions.FirstOrDefault(p => p.id == recNomID);
            if(instance != null)
            {
                Db.dirRegions.DeleteOnSubmit(instance);
                try
                {
                    Db.dirRegions.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.dirRegions.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public dirRegions getDirRegionByID(int orgID)
        {
            return Db.dirRegions.FirstOrDefault(p => p.id == orgID);
        }

        public dirRegions getDirRegionOrg(int orgID)
        {
            return Db.dirOrg.FirstOrDefault(p => p.id == orgID).dirRegions;
        }
    }
}
