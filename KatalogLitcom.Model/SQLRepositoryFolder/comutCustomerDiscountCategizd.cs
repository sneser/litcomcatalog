﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogLitcom.Model
{
    public partial class SQLRepository
    {
        public IQueryable<comutCustomerDiscountCategizd> comutCustomerDiscountCategizds
        {
            get
            {
                return Db.comutCustomerDiscountCategizd;
            }
        }

        public bool createNewcomutCustomerDiscountCategizd(comutCustomerDiscountCategizd instanse)
        {
            if(instanse.comutID == 0)
            {
                Db.comutCustomerDiscountCategizd.InsertOnSubmit(instanse);
                try
                {
                    Db.comutCustomerDiscountCategizd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.comutCustomerDiscountCategizd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool updatecomutCustomerDiscountCategizd(comutCustomerDiscountCategizd instanse)
        {
            comutCustomerDiscountCategizd cache = Db.comutCustomerDiscountCategizd.FirstOrDefault(p => p.comutID == instanse.comutID);
            if(cache != null)
            {
                cache.categoryForSkladIzd = instanse.categoryForSkladIzd;
                cache.dirDiscountCustomer = instanse.dirDiscountCustomer;
                cache.dirOrg = instanse.dirOrg;
                try
                {
                    Db.comutCustomerDiscountCategizd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.comutCustomerDiscountCategizd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }

        public bool deletecomutCustomerDiscountCategizd(int mappingID)
        {
            comutCustomerDiscountCategizd instance = Db.comutCustomerDiscountCategizd.FirstOrDefault(p => p.comutID == mappingID);
            if(instance != null)
            {
                Db.comutCustomerDiscountCategizd.DeleteOnSubmit(instance);
                try
                {
                    Db.comutCustomerDiscountCategizd.Context.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    Db.ChangeConflicts.ResolveAll(RefreshMode.KeepChanges);
                    {
                        try
                        {
                            Db.comutCustomerDiscountCategizd.Context.SubmitChanges();
                        }
                        catch (ChangeConflictException e)
                        {
                            logger.Error(e.Message);
                            return false;
                        }
                        return true;
                    }
                }
                return true;
            }
            return false;
        }
        
        public comutCustomerDiscountCategizd getComutCustomerDiscountCategizdByID(int mappingID)
        {
            return Db.comutCustomerDiscountCategizd.FirstOrDefault(p => p.comutID == mappingID);
        }
    }
}
