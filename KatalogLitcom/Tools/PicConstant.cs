﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;


namespace Footwear
{
    public static class PicConstant
    {
        public const string PICTURE_PATH = @"\Pictures";

        public static string createFileNamePic(string prodArticle, string pathFile)
        {
            string picFileName = pathFile + @"\" + prodArticle + "0.jpg";
            if (File.Exists(picFileName))
            {
                int i = 0;
                while (File.Exists(picFileName))
                {
                    i++;
                    string str1 = i.ToString() + ".jpg";
                    picFileName = pathFile + @"\" + prodArticle + str1;
                }
            }
            return picFileName;
        }

        public static Image Zoom(Image imgPhoto, int maxWidth, int maxHeight)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            if (sourceWidth <= maxWidth && sourceHeight <= maxHeight)
                return imgPhoto;
            float nPercent = 0;
            float nPercentW = ((float)maxWidth / (float)sourceWidth);
            float nPercentH = ((float)maxHeight / (float)sourceHeight);

            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
            }
            else
            {
                nPercent = nPercentW;
            }
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);
            Bitmap bmPhoto = new Bitmap(destWidth,
                    destHeight, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                    imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode =
                    InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(0, 0, destWidth, destHeight),
                new Rectangle(0, 0, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }
    }
}
