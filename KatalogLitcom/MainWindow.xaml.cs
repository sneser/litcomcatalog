﻿using KatalogLitcom.ViewModels.ViewModels;
using System.Windows;

namespace KatalogLitcom
{


    public partial class MainWindow : Window
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public TreeViewModel viewModel;

        public MainWindow()
        {
            viewModel = new TreeViewModel();            
            DataContext = viewModel;

            InitializeComponent();            
        }
    }
}
