﻿using Ninject.Modules;
using KatalogLitcom.Model;
using System.Configuration;

namespace KatalogLitcom.AppStart
{
    public class KatalogLitkomNinjectModule: NinjectModule
    {
        public override void Load()
        {
            Bind<kataloLitcomDataContext>().ToMethod(c => new kataloLitcomDataContext(ConfigurationManager.ConnectionStrings["KatalogLitcomConnectionString"].ConnectionString));
            Bind<IRepository>().To<SQLRepository>().InSingletonScope();
        }
    }
}
