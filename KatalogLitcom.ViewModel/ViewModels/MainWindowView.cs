﻿using KatalogLitcom.Model;
using System;
using System.Linq;
using KatalogLitcom.AppStart;
using KatalogLitcom.ViewModels.BaseClass;
using Ninject;
using KatalogLitcom.Controllers;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;

namespace KatalogLitcom.ViewModels.ViewModels
{
    
    public class TreeViewModel : ViewModel
    {
        public IRepository Repository;

        private DataGridView _currentselectedizd;

        public TreeViewModel()
        {
            Repository = (new StandardKernel(new KatalogLitkomNinjectModule())).Get<BaseController>().Repository;

            TopLavelItems = new ObservableCollection<TreeViewItemModel>();
            dgKatalogIzd = new ObservableCollection<DataGridView>();
            SetTopLavelItems();
            SetCatalogTable();

        }

        /*var tmpCustDiscount = Repository.dirDiscountCustomers.Select(p => p).Join(Repository.comutCustomerDiscountCategizds.Where(c => c.customerID == custID),
                                                                                      a => a.discountID,
                                                                                      p => p.discountID,
                                                                                      (a, p) => new
                                                                                      {
                                                                                          a.discontName
                                                                                      }).Distinct().ToList();*/

        private void SetCatalogTable()
        {

            //var tmp

            var tmpIzds = Repository.dirIzds.Select(p => p);/*.Join(Repository.comutStatusIzds.Select(c => c),
                                                                              a => a.id,
                                                                              p => p.id_Izd,
                                                                              (a, p) => new {
                                                                                  a.id,
                                                                                  a.nameIzd,
                                                                                  a.myIndex,
                                                                                  p.dirStatusPartIsd.statuscomplete,
                                                                                  a.sizeIzd,
                                                                                  a.imgFile
                                                                              })
                                                             .Join(Repository.);*/

            foreach(var itemIzd in tmpIzds)
            { 
                dgKatalogIzd.Add(new DataGridView()
                {
                    Art = itemIzd.id,
                    NameIzd = itemIzd.nameIzd + " " + itemIzd.myIndex,
                    IndexIzd = itemIzd.myIndex,
                    SizeIzd = itemIzd.sizeIzd,
                    ImgPath = itemIzd.imgFile
                });
            }
        }

        public ObservableCollection<TreeViewItemModel> TopLavelItems { get; set; }

        public ObservableCollection<DataGridView> dgKatalogIzd { get; set; }

        public DataGridView curentSelectedIzd
        {
            get
            {
                return _currentselectedizd;
            }
            set
            {
                _currentselectedizd = value;
                var tmpIzd = Repository.getIzdByID(_currentselectedizd.Art);
                dgKatalogIzd.Add(new DataGridView()
                {
                    Art = _currentselectedizd.Art,
                    curentSelectedIzd = tmpIzd,
                    Description = _currentselectedizd.Description,
                    ImgGaleryPath = _currentselectedizd.ImgGaleryPath,
                    ImgPath = _currentselectedizd.ImgPath,
                    IndexIzd = _currentselectedizd.IndexIzd,
                    NameIzd = _currentselectedizd.NameIzd,
                    Price = _currentselectedizd.Price,
                    SizeIzd = _currentselectedizd.SizeIzd,
                    Weigth = _currentselectedizd.Weigth
                });
                OnPropertyChanged("currentSelectedIzd");
            }
        }



        private void SetTopLavelItems()
        {
            var categories = Repository.categoryForSkladIzds.Select(p => p);

            ObservableCollection<TreeViewItemModel> _subcateg = new ObservableCollection<TreeViewItemModel>();

            _subcateg.Add(new TreeViewItemModel() { CategoryName = "Нет вложенных подкатегорий!" });

            foreach (var categ in categories)
            {
                TopLavelItems.Add(new TreeViewItemModel()
                {
                    CategoryName = categ.categoryName,
                    recID = categ.id,
                    subCategories = _subcateg
                });
            }
        }
    }
}

    