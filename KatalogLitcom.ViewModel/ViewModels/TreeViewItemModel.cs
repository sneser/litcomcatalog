﻿using KatalogLitcom.AppStart;
using KatalogLitcom.Controllers;
using KatalogLitcom.Model;
using KatalogLitcom.ViewModels.BaseClass;
using KatalogLitcom.ViewModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogLitcom.ViewModels.ViewModels
{
    public class TreeViewItemModel : ViewModel
    {
        private IRepository Repository;

        private int _recid;

        private string _categoryname;

        private bool _isSelected;

        private bool _isExpanded;

        public ObservableCollection<TreeViewItemModel> subCategories
        {
            get; set;
        }

        public TreeViewItemModel()
        {
            Repository = (new StandardKernel(new KatalogLitkomNinjectModule())).Get<BaseController>().Repository;
            subCategories = new ObservableCollection<TreeViewItemModel>();
        }

        public int recID
        {
            get { return _recid; }
            set { _recid = value; OnPropertyChanged("recID"); }
        }

        public string CategoryName
        {
            get
            {
                return _categoryname;
            }
            set
            {
                _categoryname = value; OnPropertyChanged("CategoryName");
            }
        }

        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        public bool IsExpanded
        {
            get
            {
                return _isExpanded;
            }
            set
            {
                _isExpanded = value;
                OnPropertyChanged("IsExpanded");
                if (_isExpanded)
                {
                    var subCateg = Repository.getCategoryByParentID(recID);
                    if (subCateg.Count != 0)
                    {
                        subCategories.Clear();
                        foreach (var item in subCateg)
                        {
                            subCategories.Add(new TreeViewItemModel()
                            {
                                CategoryName = item.categoryName
                            });
                        }
                    }
                    else
                    {
                        subCategories.Clear();
                        subCategories.Add(new TreeViewItemModel()
                        {
                            CategoryName = "Нет вложенных подкатегорий!"
                        });
                    }
                }
            }
        }

    }
}
