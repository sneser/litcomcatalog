﻿using KatalogLitcom.Model;
using KatalogLitcom.ViewModels.BaseClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogLitcom.ViewModels.ViewModels
{
    public class DataGridView : ViewModel
    {
        private string _nameizd;

        private int _art;

        private int _catalogid;

        private string _satatusizd;

        private string _indexizd;

        private string _sizeizd;

        private double _weigth;

        private double _price;

        private string _imgpath;

        private string _imggalerypath;

        private string _description;

        private dirIzd _currentselectedizd;

        public int Art
        {
            get
            {
                return _art;
            }
            set
            {
                _art = value;
                OnPropertyChanged("Art");
            }
        }

        public int CatalogID
        {
            get
            {
                return _catalogid;
            }
            set
            {
                _catalogid = value;
                OnPropertyChanged("CatalogID");
            }
        }

        public string NameIzd
        {
            get
            {
                return _nameizd;
            }
            set
            {
                _nameizd = value;
                OnPropertyChanged("NameIzd");
            }
        }

        public string StatusIzd
        {
            get
            {
                return _satatusizd;
            }
            set
            {
                _satatusizd = value;
                OnPropertyChanged("StatusIzd");
            }
        }

        public string IndexIzd
        {
            get
            {
                return _indexizd;
            }
            set
            {
                _indexizd = value;
                OnPropertyChanged("IndexIzd");
            }
        }

        public string SizeIzd
        {
            get
            {
                return _sizeizd;
            }
            set
            {
                _sizeizd = value;
                OnPropertyChanged("SizeIzd");
            }
        }

        public double Weigth
        {
            get
            {
                return _weigth;
            }
            set
            {
                _weigth = value;
                OnPropertyChanged("Weigth");
            }
        }

        public double Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
                OnPropertyChanged("Price");
            }
        }

        public string ImgPath
        {
            get
            {
                return _imgpath;
            }
            set
            {
                _imgpath = value;
                OnPropertyChanged("ImgPath");
            }
        }

        public string ImgGaleryPath
        {
            get
            {
                return _imggalerypath;
            }
            set
            {
                _imggalerypath = value;
                OnPropertyChanged("ImgGaleryPath");
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                OnPropertyChanged("Description");
            }
        }

        public dirIzd curentSelectedIzd
        {
            get
            {
                return _currentselectedizd;
            }
            set
            {
                _currentselectedizd = value;
                OnPropertyChanged("currentSelectedIzd");
            }
        }

    }
}
