﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatalogLitcom.ViewModels.BaseClass
{
    public class ViewModel : ObservableObject, IDataErrorInfo
    {
        public string this[string columnName]
        {
            get
            {
                return OnValidate(columnName);
            }
        }

        private string OnValidate(string propertyName)
        {
            var context = new ValidationContext(this)
            {
                MemberName = propertyName
            };
            var results = new Collection<ValidationResult>();
            bool isValid = Validator.TryValidateObject(this, context, results, true);

            if (!isValid)
            {
                ValidationResult result = results.SingleOrDefault(r => r.MemberNames.Any(m => m == propertyName));

                return result == null ? null : result.ErrorMessage;
            }
            return null;
        }
        
        [Obsolete]
        public string Error
        {
            get
            {
                throw new NotSupportedException();
            }
        }
    }
}
