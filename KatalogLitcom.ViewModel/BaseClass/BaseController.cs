﻿using Ninject;
using KatalogLitcom.Model;

namespace KatalogLitcom.Controllers
{
    public class BaseController
    {
        [Inject]
        public IRepository Repository { get; set; }
    }
}
